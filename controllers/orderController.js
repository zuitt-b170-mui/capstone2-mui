const User = require("../models/user")
const Product = require("../models/product")
const Order = require("../models/order")
const auth = require("../auth")
const bcrypt = require("bcrypt")

// checkout
// module.exports.userOrder = (reqBody, userData) => {
//     return User.findById(userData.userId).then(result => {
//         if(userData.isAdmin == false){
//             return "You are not an admin"
//         }else{
//             let newOrder = new Order({
//                 totalAmount: reqBody.totalAmount
//             })
//             return newOrder.save().then((order, error) => {
//                 if(error){
//                     return false
//                 }else{
//                     return "Checkout successful"
//                 }
//             })
//         }
//     })
// }

// checkout
module.exports.userOrder = (reqBody, userData) => {
    let newOrder = new Order({
        totalAmount: reqBody.totalAmount
    })
    return newOrder.save().then((order, error) => {
        if(error){
            return false
        }else{
            return "Checkout successful"
        }
    })
}
// retrieve all orders
module.exports.getAllOrders = () => {
    return Order.find({}).then((result,error) => {
        if(error){
            return false
        }else{
            return result
        }
    })
}
// user orders