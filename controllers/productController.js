const User = require("../models/user")
const Product = require("../models/product")
const Order = require("../models/order")
const auth = require("../auth")
const bcrypt = require("bcrypt")

// retrieve all active products
module.exports.getActiveProducts = () => {
    return Product.find( {isActive: true} ).then(result => {
        return result
    })
}

// retrieve a product
module.exports.getProduct = (reqParams) =>{
    return Product.findById(reqParams.productId).then(result =>{
        return result
    })
}

// create a product
module.exports.addProduct = (reqBody, userData) => {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
            return newProduct.save().then((product, error) => {
                if(error){
                    return false
                }else{
                    return "Product creation successful"
                }
            })
}

// update product information
module.exports.updateProduct = (reqParams, reqBody) => {
    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result,error) => {
        if(error){
            return false
        }else{
            return true
        }
    })
}

// archive product
module.exports.archiveProduct = (reqParams, reqBody) => {
    let updatedProduct = {
        isActive: reqBody.isActive
    }
    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((result,error) => {
        if(error){
            return false
        }else{
            return true
        }
    })
}