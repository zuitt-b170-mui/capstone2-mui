const express = require("express")
const router = express.Router()

const auth = require("../auth")
const orderController = require("../controllers/orderController")
const order = require("../models/order")

// checkout
router.post("/checkout", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    orderController.userOrder(req.body, userData).then(resultFromController => res.send(resultFromController))
})
// retrieve all orders
router.get("/orders", auth.verify, (req,res) => {
    orderController.getAllOrders().then(resultFromController => res.send(resultFromController))
})
// user orders

module.exports = router;