const express = require("express")
const router = express.Router()

const auth = require("../auth")
const userController = require("../controllers/userController")
const user = require("../models/user")

// register user
router.post("/register", (req,res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// user login
router.post("/login", (req,res) => {
    userController.userLogin(req.body).then(resultFromController => res.send(resultFromController))
})

// user details
router.get("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    userController.getProfile({userId:userData.id}).then(resultFromController => res.send(resultFromController))
})

// update user to admin
router.put("/update", auth.verify, (req,res) => {
    userController.updateUser(req.params, req.body).then(result => res.send(result))
})

module.exports = router;